const offers = [
	{
		cover: "https://picsum.photos/345/140",
		name: "Nombre del producto",
		description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt",
		expiration: "31/02"
	}
];
// {
//   cover: "https://picsum.photos/345/143",
//   name: "Nombre del otro producto",
//   description:
//     "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt",
//   expiration: "14/12"
// }

export { offers };
