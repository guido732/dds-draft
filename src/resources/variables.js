const toRem = unit => {
	const unitConverted = parseInt(unit);
	return `${unitConverted / 16}rem`;
};

const customColors = {
	brandColors: {
		blue: {
			base: "#6CB1E6",
			focus: "#6CB1E6",
			"100": "#D9F0FF",
			"200": "#A3D3F7",
			"300": "#6CB1E6",
			"400": "#368ACC",
			"500": "#0060A9",
			"600": "#004E96",
			"700": "#003B7A",
			"800": "#002654",
			"900": "#001026"
		},
		yellow: {
			base: "#F7C137",
			focus: "#F7C137",
			"100": "#FFFCD9",
			"200": "#FFF4A3",
			"300": "#FFE770",
			"400": "#FFD64F",
			"500": "#F7C137",
			"600": "#C9A018",
			"700": "#937B04",
			"800": "#5C5100",
			"900": "#262300"
		},
		gray: {
			"100": "#F9F9FB",
			"200": "#F0F1F5",
			"300": "#E8EAEF",
			"400": "#D8DAE4",
			"500": "#BEC1CC",
			"600": "#A3A8B6",
			"700": "#888E9F",
			"800": "#515B70",
			"900": "#1B293F"
		},
		white: {
			"500": "#FFFFFF",
			"700": "#FCFEFF"
		},
		black: {
			"500": "#1F1F1F",
			"900": "#171717"
		}
	},
	interfaceColors: {
		red: {
			"100": "#FFD9D9",
			"200": "#FFA3A3",
			"300": "#FF6C6E",
			"400": "#FF434A",
			"500": "#E8282F",
			"600": "#C90D14",
			"700": "#930001",
			"800": "#5C0000",
			"900": "#260000"
		},
		purple: {
			"100": "#E9D9FF",
			"200": "#CDAAFF",
			"300": "#B484FF",
			"400": "#9F68FF",
			"500": "#8C54FF",
			"600": "#6831C9",
			"700": "#481893",
			"800": "#2B075C",
			"900": "#110026"
		},
		green: {
			"100": "#D9FCD9",
			"200": "#A5F2A3",
			"300": "#79E175",
			"400": "#53CA4E",
			"500": "#33AC2E",
			"600": "#1D9418",
			"700": "#0D7508",
			"800": "#035000",
			"900": "#002300"
		},
		pink: {
			"100": "#FFD9E5",
			"200": "#FFA3C3",
			"300": "#FF70A4",
			"400": "#F84C88",
			"500": "#E0326E",
			"600": "#C21651",
			"700": "#930438",
			"800": "#5C0021",
			"900": "#26000C"
		}
	},
	gradientColors: {
		aliadas: {
			background: "linear-gradient(75.4deg, #FFD9E5 9.81%, #E0326E 90.19%);"
		},
		blue: {
			background: "linear-gradient(118.74deg, #0060A9 15.44%, #481893 102.12%)"
		},
		red: {
			background: "linear-gradient(135deg, #FF434A 30.48%, #930001 96.77%)"
		},
		lightBlue: {
			background: "linear-gradient(170.49deg, #FBFEFF 0%, #A8D6E7 157.5%)"
		}
	}
};

const spacing = {
	vertical: {
		"0.5": toRem("4px"),
		"1": toRem("8px"),
		"2": toRem("16px"),
		"3": toRem("24px"),
		"4": toRem("32px")
	},

	horizontal: {
		"0.5": toRem("4px"),
		"1": toRem("8px"),
		"2": toRem("16px"),
		"3": toRem("24px"),
		"4": toRem("32px"),
		"5": toRem("40px"),
		"6": toRem("48px"),
		"7": toRem("56px"),
		"8": toRem("80px")
	}
};

const typography = {
	headers: {
		normal: {
			fontStyle: "normal",
			fontWeight: 400
		},
		medium: {
			fontWeight: 500
		},
		h1: {
			fontSize: toRem("48px"),
			lineHeight: toRem("57px")
		},
		h2: {
			fontSize: toRem("40px"),
			lineHeight: toRem("47px")
		},
		h3: {
			fontSize: toRem("32px"),
			lineHeight: toRem("38px")
		},
		h4: {
			fontSize: toRem("24px"),
			lineHeight: toRem("28px")
		}
	},
	body: {
		normal: {
			fontStyle: "normal",
			fontWeight: 400
		},
		medium: {
			fontWeight: 500
		},
		lg: {
			fontSize: toRem("24px"),
			lineHeight: toRem("30px")
		},
		md: {
			fontSize: toRem("16px"),
			lineHeight: toRem("24px")
		},
		sm: {
			fontSize: toRem("12px"),
			lineHeight: toRem("16px")
		},
		list: {
			fontWeight: 300,
			fontSize: toRem("12px"),
			lineHeight: toRem("16px"),
			letterSpacing: "0.03em"
		},
		caption: {
			fontSize: toRem("12px"),
			lineHeight: toRem("14px"),
			textTransform: "uppercase"
		}
	}
};

const utils = {
	title: {
		fontWeight: 400,
		fontSize: toRem("16px"),
		color: customColors.brandColors.blue[500],
		marginBottom: toRem("8px")
	},

	cardShadow: {
		boxShadow: `0 ${toRem("10px")} ${toRem("20px")} rgba(0, 90, 159, 0.05)`,
		border: `${toRem("1px")} solid ${customColors.brandColors.gray[300]}`
	}
};

export { customColors, spacing, typography, utils };
