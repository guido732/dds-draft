## Component Library

The component library used is [Material UI](material-ui.com/)

## Icon Library

The icon library used is [Feather Icons](https://feathericons.com/) <br>
The project uses [React-Feather](https://github.com/feathericons/react-feather) by Feather Icons to import them as components to maintain native functionality of MUI Icons in components
