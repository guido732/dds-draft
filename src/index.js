import React from "react";
import ReactDOM from "react-dom";
// import * as serviceWorker from "./serviceWorker";

import { createMuiTheme, Typography, Divider, Tooltip, Link } from "@material-ui/core";
import { ThemeProvider } from "@material-ui/styles";
import { Camera } from "react-feather";

import PrimaryButton from "./components/PrimaryButton";
import OfferCard from "./components/cards/OfferCard";
import CustomAvatars from "./components/CustomAvatars";
import CustomAvatarGroup from "./components/CustomAvatarGroup";
import TextBlock from "./components/TextBlock";

import { offers } from "./mock/data";
import { customColors } from "./resources/variables";

const theme = createMuiTheme({
	typography: {
		fontFamily: ["Rubik", "monospace"].join(",")
	}
});

const onButtonClick = () => {
	console.log("Botón de prueba");
};

const handleAddButton = () => {
	console.log("Agregar al carrito");
};

function App() {
	return (
		<ThemeProvider theme={theme}>
			<TextBlock variant="h1" bold>
				H1 Bold
			</TextBlock>
			<br />
			<TextBlock variant="h1" size="lg" light>
				H1 Light (con size="lg" que no hace nada)
			</TextBlock>
			<br />
			<TextBlock variant="h2" bold>
				<Link color="inherit" variant="inherit" href="www.google.com" target="_blank" rel="noopener noreferer">
					H2 en Bold con Anchor element que hereda estilos
				</Link>
			</TextBlock>
			<br />
			<TextBlock variant="h3" customColor="blue" bold>
				H2 en Bold con Anchor element que hereda estilos
			</TextBlock>
			<br />
			<TextBlock variant="body1" size="lg">
				Body1 size="lg"
			</TextBlock>
			<br />
			<TextBlock variant="body1" bold size="perro">
				Body1 size="perro" o cualquier otro valor + Bold
			</TextBlock>
			<br />
			<TextBlock variant="body1" size="perro">
				Body1 size="perro" o cualquier otro valor
			</TextBlock>
			<br />
			<TextBlock variant="body1" list>
				Body1 size="perro" o cualquier otro valor + list
			</TextBlock>
			<br />
			<TextBlock variant="body1" size="sm">
				Body1 size="sm"
			</TextBlock>
			<br />
			<TextBlock variant="body2" size="sm">
				Body2 size="sm" (no importa tamaño) - importa el Bold
			</TextBlock>
			<br />
			<TextBlock variant="body2" bold>
				Body2 size="sm" (no importa tamaño) - importa el Bold
			</TextBlock>
			<br />
			<TextBlock variant="caption">Caption - no importa tamaño, solo bold o no</TextBlock>
			<br />
			<TextBlock variant="caption" bold>
				Caption - no importa tamaño, solo bold o no
			</TextBlock>
			<br />

			{/* <Typography variant="h4" style={{ marginBottom: "1rem" }}>
				Botones
			</Typography> */}
			<CustomAvatars>HC</CustomAvatars>
			<CustomAvatarGroup>
				<CustomAvatars alt="Remy Sharp" src="/static/images/avatar/1.jpg" />
				<CustomAvatars alt="Travis Howard" src="/static/images/avatar/2.jpg" />
				<CustomAvatars alt="Cindy Baker" src="/static/images/avatar/3.jpg" />
				<Tooltip title="Foo • Bar • Baz">
					<CustomAvatars>+3</CustomAvatars>
				</Tooltip>
			</CustomAvatarGroup>
			<PrimaryButton variant="outlined" size="large" onClick={onButtonClick} startIcon={<Camera />}>
				Outlined
			</PrimaryButton>
			<PrimaryButton customColor={customColors.brandColors.yellow} variant="outlined" onClick={onButtonClick}>
				Outlined
			</PrimaryButton>
			<PrimaryButton variant="outlined" size="small" disabled onClick={onButtonClick}>
				Outlined
			</PrimaryButton>
			<br />
			<br />
			<PrimaryButton variant="contained" size="large" onClick={onButtonClick} startIcon={<Camera />}>
				Contained
			</PrimaryButton>
			<PrimaryButton customColor={customColors.brandColors.yellow} variant="contained" onClick={onButtonClick}>
				Contained
			</PrimaryButton>
			<PrimaryButton variant="contained" size="small" disabled onClick={onButtonClick}>
				Contained
			</PrimaryButton>
			<br />
			<br />
			<PrimaryButton variant="text" onClick={onButtonClick}>
				Text
			</PrimaryButton>
			<PrimaryButton customColor={customColors.brandColors.yellow} variant="text" onClick={onButtonClick}>
				Text
			</PrimaryButton>
			<PrimaryButton variant="text" disabled onClick={onButtonClick}>
				Text
			</PrimaryButton>
			<Divider style={{ margin: "1rem 0" }} />
			<Typography variant="h4" style={{ marginBottom: "1rem" }}>
				Cards
			</Typography>
			{offers.map(offer => {
				return <OfferCard data={offer} handleAddButton={handleAddButton} />;
			})}
			<Divider style={{ margin: "2rem 0" }} />
		</ThemeProvider>
	);
}

ReactDOM.render(<App />, document.getElementById("root"));

// serviceWorker.unregister();
