import { withStyles } from "@material-ui/styles";
import AvatarGroup from "@material-ui/lab/AvatarGroup";

const CustomAvatarGroup = withStyles({
	root: {
		marginBottom: "2rem"
	}
})(AvatarGroup);

export default CustomAvatarGroup;
