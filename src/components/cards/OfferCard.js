import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import { customColors, utils } from "../../resources/variables";
import PrimaryButton from "../PrimaryButton";
import { Grid } from "@material-ui/core";
import TextBlock from "../TextBlock";

const useStyles = makeStyles({
	card: {
		maxWidth: 304,
		borderRadius: "5",
		...utils.cardShadow
	},
	media: {
		height: 152
	}
});

export default function OfferCard({ data, handleAddButton }) {
	const classes = useStyles();

	return (
		<Card className={classes.card}>
			<CardMedia className={classes.media} image={data.cover} title="Lorem ipsum" />
			<CardContent style={{ padding: 16 }}>
				<TextBlock align="center" variant="body1" size="md" customColor={customColors.brandColors.blue[500]}>
					{data.name}
				</TextBlock>
				<TextBlock align="center" variant="body1" size="sm" customColor={customColors.brandColors.gray[700]}>
					{data.description}
				</TextBlock>
				<TextBlock align="center" variant="body1" size="sm" customColor={customColors.brandColors.gray[500]}>
					Válido hasta: {data.expiration}
				</TextBlock>
				<Grid align="center">
					<PrimaryButton
						customColor={customColors.brandColors.secondaryColor}
						size="large"
						variant="contained"
						onClick={handleAddButton}
					>
						Agregar al carrito
					</PrimaryButton>
				</Grid>
			</CardContent>
		</Card>
	);
}
