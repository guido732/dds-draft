import { withStyles } from "@material-ui/styles";
import Avatar from "@material-ui/core/Avatar";

const customAvatar = withStyles({})(Avatar);

export default customAvatar;
