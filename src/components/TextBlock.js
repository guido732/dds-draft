import { withStyles } from "@material-ui/styles";
import { typography, customColors } from "../resources/variables";
import { Typography } from "@material-ui/core";

const TextBlock = withStyles({
	root: {
		fontFamily: ["Rubik", "Monospace"].join(", "),
		fontStyle: typography.headers.normal.fontStyle,
		color: ({ customColor }) => (customColor ? customColor : customColors.brandColors.black[500])
	},
	h1: {
		fontWeight: ({ bold }) => (bold ? typography.headers.medium.fontWeight : typography.headers.normal.fontWeight),
		fontSize: typography.headers.h1.fontSize,
		lineHeight: typography.headers.h1.lineHeight
	},
	h2: {
		fontWeight: ({ bold }) => (bold ? typography.headers.medium.fontWeight : typography.headers.normal.fontWeight),
		fontSize: typography.headers.h2.fontSize,
		lineHeight: typography.headers.h2.lineHeight
	},
	h3: {
		fontWeight: ({ bold }) => (bold ? typography.headers.medium.fontWeight : typography.headers.normal.fontWeight),
		fontSize: typography.headers.h3.fontSize,
		lineHeight: typography.headers.h3.lineHeight
	},
	h4: {
		fontWeight: ({ bold }) => (bold ? typography.headers.medium.fontWeight : typography.headers.normal.fontWeight),
		fontSize: typography.headers.h4.fontSize,
		lineHeight: typography.headers.h4.lineHeight
	},
	body1: {
		fontWeight: ({ bold }) => (bold ? typography.body.medium.fontWeight : typography.body.normal.fontWeight),
		fontSize: ({ size }) => {
			switch (size) {
				case "lg":
					return typography.body.lg.fontSize;
				case "sm":
					return typography.body.sm.fontSize;
				default:
					return typography.body.md.fontSize;
			}
		},
		lineHeight: ({ size }) => {
			switch (size) {
				case "lg":
					return typography.body.lg.lineHeight;
				case "sm":
					return typography.body.sm.lineHeight;
				default:
					return typography.body.md.lineHeight;
			}
		}
	},
	body2: {
		fontSize: typography.body.list.fontSize,
		lineHeight: typography.body.list.lineHeight,
		letterSpacing: ({ list }) => (list ? typography.body.list.letterSpacing : "initial"),
		fontWeight: ({ bold }) => (bold ? typography.body.medium.fontWeight : typography.body.list.fontWeight)
	},
	caption: {
		fontSize: typography.body.caption.fontSize,
		lineHeight: typography.body.caption.lineHeight,
		textTransform: typography.body.caption.textTransform,
		fontWeight: ({ bold }) => (bold ? typography.body.medium.fontWeight : typography.body.normal.fontWeight)
	}
})(Typography);

export default TextBlock;
