import { withStyles } from "@material-ui/styles";
import { customColors, spacing } from "../resources/variables";
import { Button } from "@material-ui/core";

const PrimaryButton = withStyles({
	root: {
		boxSizing: "border-box",
		textTransform: "none",
		borderRadius: spacing.horizontal[3],
		fontWeight: "400",
		color: customColors.brandColors.white[500],
		paddingLeft: spacing.horizontal[4],
		paddingRight: spacing.horizontal[4],
		boxShadow: "none",
		"&:hover": {
			background: ({ customColor }) => (customColor ? customColor[200] : customColors.brandColors.blue[200])
		},
		"&:disabled": {
			color: customColors.brandColors.gray[500]
		}
	},
	text: {
		backgroundColor: "transparent !important",
		color: ({ customColor }) => (customColor ? customColor.base : customColors.brandColors.blue[500]),
		"&:hover": {
			backgroundColor: "transparent !important"
		}
	},
	contained: {
		backgroundColor: ({ customColor }) => (customColor ? customColor[500] : customColors.brandColors.blue[300]),
		"&:hover": {
			boxShadow: "none",
			backgroundColor: ({ customColor }) => (customColor ? customColor[400] : customColors.brandColors.blue[200])
		},
		"&:disabled": {
			backgroundColor: customColors.brandColors.gray[200]
		}
	},
	outlined: {
		border: ({ customColor }) => `2px solid ${customColor ? customColor[500] : customColors.brandColors.blue[300]}`,
		color: ({ customColor }) => (customColor ? customColor[500] : customColors.brandColors.blue[300]),
		paddingLeft: "calc(2rem - 2px)",
		paddingRight: "calc(2rem - 2px)",
		backgroundColor: "transparent !important",
		"&:hover": {
			backgroundColor: "transparent !important",
			border: ({ customColor }) => `2px solid ${customColor ? customColor[400] : customColors.brandColors.blue[200]}`,
			color: ({ customColor }) => (customColor ? customColor[400] : customColors.brandColors.blue[200])
		},
		"&:disabled": {
			borderWidth: "2px",
			borderColor: customColors.brandColors.gray[200]
		}
	},
	containedSizeSmall: {
		paddingTop: spacing.vertical[0.5],
		paddingBottom: spacing.vertical[0.5]
	},
	containedSizeLarge: {
		paddingTop: spacing.vertical[1],
		paddingBottom: spacing.vertical[1]
	},
	outlinedSizeSmall: {
		paddingTop: spacing.vertical[0.5],
		paddingBottom: spacing.vertical[0.5]
	},
	outlinedSizeLarge: {
		paddingTop: spacing.vertical[1],
		paddingBottom: spacing.vertical[1]
	},
	textSizeSmall: {
		paddingTop: spacing.vertical[0.5],
		paddingBottom: spacing.vertical[0.5]
	},
	textSizeLarge: {
		paddingTop: spacing.vertical[1],
		paddingBottom: spacing.vertical[1]
	}
})(Button);

export default PrimaryButton;
